package com.example.demo.api;

import com.example.demo.model.Person;
import com.example.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("api/v1/person") // JSON payload from Postman
@RestController
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    // Types of methods sent to the backend:
    // GET - retrieving data from server,
    // POST - adding resource to server,
    // PUT - modifying resource,
    // DELETE - deleting resource from server
    // addPerson method below serves as a POST request, aka it adds a person (resource) to the server
    @PostMapping // set as POST method
    // Receives JSON payload from the JSON body and then shovels (aka assign) into Person object
    public void addPerson(@Valid @NonNull @RequestBody Person person) {
        personService.addPerson(person);
    }

    // getAllPeople is a GET method
    @GetMapping
    public List<Person> getAllPeople() {
        return personService.getAllPeople();
    }

    // Search for a person by ID, requires setting as PathVariable
    // Is a type of GET method, thus @GetMapping is added
    @GetMapping(path = "{id}")         // Grab id from request, set to as PathVariable
    public Person getPersonById(@PathVariable("id") UUID id) {
        return personService.getPersonById(id)
                .orElse(null); // Optional: Add a response to show not found error.
    }

    // Delete the person by ID, is a type of DELETE method.
    @DeleteMapping(path = "{id}")
    public void deletePerson(@PathVariable("id") UUID id) {
        personService.deletePerson(id);
    }

    // Search for ID of the person-to-update, then get newPerson and shovel it into Person.
    @PutMapping(path = "{id}")
    public void updatePersonBy(@PathVariable("id") UUID id, @Valid @NonNull @RequestBody Person newPerson) {
        personService.updatePerson(id, newPerson);
    }
}
