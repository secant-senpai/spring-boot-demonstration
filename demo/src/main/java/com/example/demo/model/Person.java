package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.internal.NotNull;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class Person {
    private final UUID id;
    // Add the following dependency and restart the IntelliJ
    // <dependency>
    //	   <groupId>org.springframework.boot</groupId>
    //	   <artifactId>spring-boot-starter-validation</artifactId>
    // </dependency>
    @NotBlank
    private final int game_level;

    // Defined by JSON
    public Person(@JsonProperty("id") UUID id,
                  @JsonProperty("game_level") int game_level) {
        this.id = id;
        this.game_level = game_level;
    }

    public UUID getId() {
        return id;
    }

    public int getGameLevel() {
        return game_level;
    }
}
