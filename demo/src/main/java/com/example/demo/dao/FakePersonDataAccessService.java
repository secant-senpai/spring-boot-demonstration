package com.example.demo.dao;

import com.example.demo.model.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDao")
public class FakePersonDataAccessService implements PersonDao {

    private static List<Person> database = new ArrayList<>();

    @Override
    public int insertPerson(UUID id, Person person) {
        database.add(new Person(id, person.getGameLevel()));
        System.out.println("added " + person.getGameLevel() + " to database.");
        return 1;
    }

    @Override
    public List<Person> selectAllPeople() {
        return database;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        return database.stream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }

    @Override
    public int deletePersonById(UUID id) {
        Optional<Person> personMaybe = selectPersonById(id);
        if (!personMaybe.isPresent()) {
            return 0;
        }
        database.remove(personMaybe.get());
        return 1;
    }

    @Override
    public int updatePersonById(UUID id, Person newPerson) {
        return selectPersonById(id)
                .map(person -> {
                    int indexOfPersonToUpdate = database.indexOf(person);
                    if (indexOfPersonToUpdate >= 0) {       // A valid person within database
                        database.set(indexOfPersonToUpdate, new Person(id, newPerson.getGameLevel()));
                        return 1;                           // Successful
                    }
                    return 0;                               // Failed
                })
                .orElse(0);                         // Failed, no such person
    }
}
